Bob Esponja 3
##############
:date: 2013-04-11 10:13
:category: Categoría
:tags: victor, paris
:author: Victor París
:summary: Lorem ipsum dolor sit amet.

Curiosidades
------------

.. image:: images/bob3.jpg

Bob Esponja es una larga serie de dibujos animados que fue diseñada para atraer a adultos y niños. Esto tiene mucho que ver con la absurda forma de vida submarina, las situaciones están representados, y con las situaciones, las referencias, y las palabras utilizadas, los espectadores más jóvenes pueden que no entiendan. Ciertas insinuaciones también están destinados a ir más allá de los espectadores más jóvenes.2 Por ejemplo, Bob Esponja haciéndole creer a su abuela que es un adulto maduro usando patillas y bombín y escuchar el free jazz o cuando Bob y Patricio piensan que Calamardo era un fantasma, un arrecife de coral esculpida como Henri de Toulouse-Lautrec. Numerosos biólogos marinos han hecho chistes marinos. El más común es cuando hay un incendio debajo del mar.

Parte de la muestra del recurso tiene que ver con la naturaleza infantil de Bob Esponja y su mejor amigo, Patricio Estrella, ambos de los cuales son adultos, pero se comportan como niños.

A diferencia de otros shows de Nickelodeon, En Bob Esponja han salidos bandas musicales que contribuyen a su banda sonora. Entre las de rock alternativo se encuentran Wilco, The Shins, The Flaming Lips and Ween, así como bandas de metal como Pantera, Motörhead y Twisted Sister han tenido apariciones en el show, y también el grupo de heavy metal Metallica. El inglés David Bowie hizo una aparición especial en SpongeBob's Atlantis SquarePantis que fue presentada el 12 de noviembre de 2007. El episodio tuvo 8.8 millones de espectadores, lo cual es la mayor audiencia en 8 años de historia.

El episodio SpongeBob's Atlantis SquarePantis hizo referencia a muchas películas. Por ejemplo, David Bowie interpretó a Su Alteza Real haciendo parodia de Yellow Submarine.
El show llegó a ser tan popular entre adolescentes y adultos que la serie se ha emitido en MTV y en Spike TV. La frase de Patricio "It's gonna rock!" del episodio Mid-Life Crustacean, fue usado como una etiqueta de línea en las promociones de estaciones de rock. En The SpongeBob SquarePants Movie, David Hasselhoff hizo un cameo.
