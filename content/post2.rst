Bob Esponja 2
##############
:date: 2013-04-11 10:12
:category: Categoría
:tags: victor, paris
:author: Victor París
:summary: Lorem ipsum dolor sit amet.

Dónde vivo
------------

.. image:: images/bob2.jpg

Fondo de Bikini es la ciudad submarina donde los personajes viven, el estereotipo de una ciudad estadounidense contaminada y superpoblada pero al fin y al cabo bonita. En Fondo de Bikini se omiten las características de la vida marina: los personajes no nadan ni flotan sino que caminan; allí nadar es lo equivalente a volar. Hay fuego, y los peces caminan usando la cola como si fueran sus piernas y las aletas como manos.

En Fondo de Bikini los caracoles marinos equivalen a los gatos en tierra firme, maúllan, y al enojarse, gritan como los gatos; los gusanos marinos equivalen a los perros, ladran, jadean, y los encadenan; las medusas equivalen a las abejas, zumban y pican; las almejas equivalen a las aves, ya que pueden volar; los nematodos a las termitas pues terminan con todo comiéndoselo; los hipocampos equivalen a caballos.
