Bob Esponja 1
##############
:date: 2013-04-11 10:11
:category: Categoría
:tags: victor, paris
:author: Victor París
:summary: Lorem ipsum dolor sit amet.

Quién soy
------------

.. image:: images/bob1.gif

En el fondo del océano Pacífico, en la ciudad submarina de Fondo de Bikini, vive el protagonista, una esponja marina rectangular y de color amarillo, llamado Bob Esponja Pantalones Cuadrados. La casa de Bob es una piña, donde vive con su mascota caracol Gary. Bob Esponja adora su trabajo como cocinero en el restaurante El Crustáceo Cascarudo o Crustáceo Crujiente y posee la habilidad de meterse en todo tipo de problemas sin quererlo. Cuando no está poniéndole los nervios de punta a Calamardo Tentáculos, su vecino calamar (irónicamente), Bob se mete en un montón de caos y experiencias raras con sus dos mejores amigos: el amigable pero tonto Patricio Estrella, una estrella de mar rosada y obesa; y Arenita Mejillas, una ardilla inteligente, fuerte pero un poco presumida, que vive en el fondo marino en su casa, un domo de poliuretano bajo el mar. Arenita sale del domo con un traje parecido al de los astronautas, para tener aire bajo el agua.
